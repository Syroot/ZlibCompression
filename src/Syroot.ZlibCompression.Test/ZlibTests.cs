using System;
using System.IO.Compression;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Syroot.ZlibCompression.Test
{
    [TestClass]
    public class ZlibTests
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly CompressionLevel[] _levels = Enum.GetValues<CompressionLevel>();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        [TestMethod]
        public void CompressDecompress()
        {
            Random random = new Random();
            CompressionLevel getRandomCompressionLevel() => _levels[random.Next(_levels.Length)];

            for (int i = 0; i < 100; i++)
            {
                byte[] originalData = new byte[random.Next(1, 1024 * 1024)];
                byte[] compressedData = Zlib.Compress(originalData, getRandomCompressionLevel());
                byte[] decompressedData = Zlib.Decompress(compressedData, originalData.Length);
                CollectionAssert.AreEqual(originalData, decompressedData);
            }
        }
    }
}
