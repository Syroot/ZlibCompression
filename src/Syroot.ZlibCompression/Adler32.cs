﻿using System;
using System.Text;

namespace Syroot.ZlibCompression
{
    /// <summary>
    /// Represents the Adler32 checksum calculation.
    /// </summary>
    public static class Adler32
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const uint _base = 65521;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Retrieves the Adler32 checksum initialized with the given <paramref name="iv"/> for the specified
        /// <paramref name="data"/>.
        /// </summary>
        /// <param name="data">The data to calculate the checksum for.</param>
        /// <param name="iv">The initial value of both Adler32 sums.</param>
        /// <returns>The Adler32 checksum resulting from the data.</returns>
        public static uint Get(ReadOnlySpan<byte> data, uint iv) => Calculate(data, iv);

        /// <summary>
        /// Retrieves the Adler32 checksum initialized with the given <paramref name="iv"/> for the specified
        /// <paramref name="data"/>.
        /// </summary>
        /// <param name="data">The text to calculate the checksum for.</param>
        /// <param name="encoding">The <see cref="Encoding"/> to retrieve the data bytes with.</param>
        /// <param name="iv">The initial value of both Adler32 sums.</param>
        /// <returns>The Adler32 checksum resulting from the data.</returns>
        public static uint Get(string data, Encoding encoding, uint iv) => Get(encoding.GetBytes(data), iv);

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static uint Calculate(ReadOnlySpan<byte> data, uint iv)
        {
            ushort sum1 = (ushort)iv;
            ushort sum2 = (ushort)(iv >> 16);
            for (int i = 0; i < data.Length; i++)
            {
                sum1 = (ushort)((sum1 + data[i]) % _base);
                sum2 = (ushort)((sum1 + sum2) % _base);
            }
            return (uint)(sum1 | (sum2 << 16));
        }
    }
}
