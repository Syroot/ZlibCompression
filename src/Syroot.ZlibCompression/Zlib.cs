﻿using System;
using System.Buffers.Binary;
using System.IO;
using System.IO.Compression;

namespace Syroot.ZlibCompression
{
    /// <summary>
    /// Represents methods for compressing or decompressing data in the ZLIB format (RFC 1950), which is the result of a
    /// deflate or inflate operation (RFC 1951) prefixed with a 2-byte header and suffixed with a 4-byte Adler32
    /// checksum of the original uncompressed data.
    /// </summary>
    public static class Zlib
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the compressed data from the given <paramref name="data"/> span.
        /// </summary>
        /// <param name="data">The buffer containing the decompressed data.</param>
        /// <param name="compressionLevel">The <see cref="CompressionLevel"/> to compress with.</param>
        /// <returns>The compressed data.</returns>
        public static unsafe byte[] Compress(ReadOnlySpan<byte> data, CompressionLevel compressionLevel)
        {
            using MemoryStream stream = new();

            // Write the compression flags.
            Span<byte> flags = stackalloc byte[sizeof(ushort)];
            BinaryPrimitives.WriteUInt16LittleEndian(flags, (ushort)GetFlags(compressionLevel));
            stream.Write(flags);

            // Write the compressed data. DeflateStream does not write a checksum.
            fixed (byte* pData = data)
            {
                using DeflateStream deflateStream = new(stream, compressionLevel, true);
                using UnmanagedMemoryStream dataStream = new(pData, data.Length);
                dataStream.CopyTo(deflateStream);
            }

            // Write the checksum.
            Span<byte> checksum = stackalloc byte[sizeof(uint)];
            BinaryPrimitives.WriteUInt32BigEndian(checksum, Adler32.Get(data, 1));
            stream.Write(checksum);
            return stream.ToArray();
        }

        /// <summary>
        /// Returns the decompressed data from the given <paramref name="data"/> span.
        /// </summary>
        /// <param name="data">The compressed data, ending with a big endian Adler32 checksum calculated from the
        /// decompressed data.</param>
        /// <param name="decompressedSize">The number of bytes to decompress.</param>
        /// <returns>The decompressed data.</returns>
        public static unsafe byte[] Decompress(ReadOnlySpan<byte> data, int decompressedSize)
        {
            byte[] decData;
            fixed (byte* pData = data)
            {
                using UnmanagedMemoryStream dataStream = new(pData, data.Length);

                // Read and validate the compression flags.
                Span<byte> flags = stackalloc byte[sizeof(ushort)];
                dataStream.Read(flags);
                if (!Enum.IsDefined((ZlibCompressionFlags)BinaryPrimitives.ReadUInt16LittleEndian(flags)))
                    throw new InvalidDataException("Invalid zlib compression flags.");

                // Read all decompressed data with DeflateStream.
                decData = new byte[decompressedSize];
                using DeflateStream deflateStream = new(dataStream, CompressionMode.Decompress);
                int totalRead = 0;
                while (totalRead < decompressedSize)
                {
                    int bytesRead = deflateStream.Read(decData.AsSpan(totalRead));
                    if (bytesRead == 0)
                        throw new InvalidDataException($"Could not decompress {decompressedSize - totalRead} bytes.");
                    totalRead += bytesRead;
                }
            }

            // Validate the checksum which DeflateStream simply read over.
            uint decompressedChecksum = BinaryPrimitives.ReadUInt32BigEndian(data[^(sizeof(uint))..]);
            uint check = Adler32.Get(decData, 1);
            if (Adler32.Get(decData, 1) != decompressedChecksum)
                throw new InvalidDataException("Mismatching Adler32 checksum.");

            return decData;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static ZlibCompressionFlags GetFlags(CompressionLevel level) => level switch
        {
            CompressionLevel.NoCompression => ZlibCompressionFlags.NoCompression,
            CompressionLevel.Fastest => ZlibCompressionFlags.Fastest,
            CompressionLevel.Optimal => ZlibCompressionFlags.Optimal,
            _ => throw new ArgumentOutOfRangeException(nameof(level))
        };

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        // Bits specifying compression method, compression info, and flags.
        private enum ZlibCompressionFlags : ushort
        {
            NoCompression = 0b00_0_00001_0111_1000,
            Fastest = 0b10_0_11100_0111_1000,
            Optimal = 0b11_0_11010_0111_1000
        }
    }
}
