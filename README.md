# ZlibCompression

This library was extracted from a larger project requiring the following missing features when using .NET's
`DeflateStream`, as it internally uses [zlib](https://zlib.net/) compression:

- Writing compressed data prefixed with zlib compression flags, followed by an adler32 checksum.
- Validating arbitrary adler32 checksums.
